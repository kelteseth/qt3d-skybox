#pragma once
#include <QObject>
#include <QString>
#include <QCoreApplication>
#include <QDebug>

class Util : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString appBasePath READ appBasePath WRITE setAppBasePath NOTIFY
                 appBasePathChanged)

  QString m_appBasePath;

public:
  explicit Util(QObject *parent = nullptr);

  QString appBasePath() const { return m_appBasePath; }

public slots:
  void setAppBasePath(QString appBasePath) {
    if (m_appBasePath == appBasePath)
      return;

    m_appBasePath = appBasePath;
    emit appBasePathChanged(m_appBasePath);
  }

signals:
  void appBasePathChanged(QString appBasePath);
};
