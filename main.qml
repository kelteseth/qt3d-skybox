import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Scene3D 2.15
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.12

Window {
    visible: true
    width: 1366
    height: 768
    title: qsTr("Skybox")
    color: "gray"

    Scene3D {
        id: scene3d
        anchors.fill: parent
        focus: true
        hoverEnabled: true
        aspects: ["input", "logic"]
        cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

        Scene {
            id: scene
        }
    }

    Item {
        id:sidebarSkyboxSettings
        width: 250

        anchors {
            top: parent.top
            right: parent.right
            bottom: parent.bottom
        }
        Rectangle {
            anchors.fill: parent
            opacity: .8
        }
        ScrollView {
            anchors.fill: parent
            anchors.margins: 10

            ScrollBar.vertical: ScrollBar {
                snapMode: ScrollBar.SnapOnRelease
            }
            ColumnLayout {
                width: sidebarSkyboxSettings.width


                TextSlider {
                    id:s1
                    text: "depolarizationFactor"
                    value: scene.sky.depolarizationFactor
                    slider.onMoved: scene.sky.depolarizationFactor = s1.slider.value
                    slider.from: 0
                    slider.to: 0.5
                }
                TextSlider {
                    id:s2
                    text: "luminance"
                    value: scene.sky.luminance
                    slider.onMoved: scene.sky.luminance = s2.slider.value
                    slider.from: 0
                    slider.to: 10
                }
                TextSlider {
                    id:s3
                    text: "mieCoefficient"
                    value: scene.sky.mieCoefficient
                    slider.onMoved: scene.sky.mieCoefficient = s3.slider.value
                    slider.from: 0
                    slider.to: 10
                }
                TextSlider {
                    id:s4
                    text: "mieDirectionalG"
                    value: scene.sky.mieDirectionalG
                    slider.onMoved: scene.sky.mieDirectionalG = s4.slider.value
                    slider.from: 0
                    slider.to: 10
                }
                TextSlider {
                    id:s5
                    text: "mieV"
                    value: scene.sky.mieV
                    slider.onMoved: scene.sky.mieV = s5.slider.value
                    slider.from: 0
                    slider.to: 10
                }
                TextSlider {
                    id:s6
                    text: "mieZenithLength"
                    value: scene.sky.mieZenithLength
                    slider.onMoved: scene.sky.mieZenithLength = s6.slider.value
                    slider.from: 0
                    slider.to: 1.25e3
                }
                TextSlider {
                    id:s7
                    text: "numMolecules"
                    value: scene.sky.numMolecules
                    slider.onMoved: scene.sky.numMolecules = s7.slider.value
                    slider.from: 0
                    slider.to: 2.542e25
                }
                TextSlider {
                    id:s8
                    text: "rayleigh"
                    value: scene.sky.rayleigh
                    slider.onMoved: scene.sky.rayleigh = s8.slider.value
                    slider.from: 0
                    slider.to: 10
                }
                TextSlider {
                    id:s9
                    text: "rayleighZenithLength"
                    value: scene.sky.rayleighZenithLength
                    slider.onMoved: scene.sky.rayleighZenithLength = s9.slider.value
                    slider.from: 0
                    slider.to: 8.4e3
                }
                TextSlider {
                    id:s10
                    text: "refractiveIndex"
                    value: scene.sky.refractiveIndex
                    slider.onMoved: scene.sky.refractiveIndex = s10.slider.value
                    slider.from: 0
                    slider.to: 10
                }
                TextSlider {
                    id:s11
                    text: "sunAngularDiameterDegrees"
                    value: scene.sky.sunAngularDiameterDegrees
                    slider.onMoved: scene.sky.sunAngularDiameterDegrees = s11.slider.value
                    slider.from: 0
                    slider.to: 1
                }
                TextSlider {
                    id:s12
                    text: "sunIntensityFactor"
                    value: scene.sky.sunIntensityFactor
                    slider.onMoved: scene.sky.sunIntensityFactor = s12.slider.value
                    slider.from: 0
                    slider.to: 10000
                }
                TextSlider {
                    id:s13
                    text: "sunIntensityFalloffSteepness"
                    value: scene.sky.sunIntensityFalloffSteepness
                    slider.onMoved: scene.sky.sunIntensityFalloffSteepness = s13.slider.value
                    slider.from: 0
                    slider.to: 10
                }
                TextSlider {
                    id:s14
                    text: "tonemapWeighting"
                    value: scene.sky.tonemapWeighting
                    slider.onMoved: scene.sky.tonemapWeighting = s14.slider.value
                    slider.from: 0
                    slider.to: 100
                }
                TextSlider {
                    id:s15
                    text: "turbidity"
                    value: scene.sky.turbidity
                    slider.onMoved: scene.sky.turbidity = s15.slider.value
                    slider.from: 0
                    slider.to: 10
                }
            }
        }
    }

    Item {
        height: 40

        anchors {
            left: parent.left
            right: sidebarSkyboxSettings.left
            bottom: parent.bottom
            margins: 20
        }
        Rectangle {
            anchors.fill: parent
            opacity: .8
        }

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 20

            Text {
                text: "Camera position: " + scene.mainCamera.position
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }
            Text {
                text: "viewCenter: " + scene.mainCamera.viewCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }
            Text {
                text: "viewVector: " + scene.mainCamera.viewVector
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }
        }
    }
}
