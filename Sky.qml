import QtQuick 2.15
import Qt3D.Core 2.15
import Qt3D.Render 2.15
import Qt3D.Input 2.15
import Qt3D.Extras 2.15

Entity {
    id: root
    components: [transform, mesh, materialSky]


    property vector3d cameraPos: camera.position
    property vector3d sunPosition: Qt.vector3d(0, -700000, 0)
    property vector3d primaries: Qt.vector3d(6.8e-7, 5.5e-7, 4.5e-7)
    property vector3d mieKCoefficient: Qt.vector3d(0.686, 0.678, 0.666)

    property real depolarizationFactor: 0.035
    property real luminance: 1
    property real mieCoefficient: 0.0034
    property real mieDirectionalG: 0.787
    property real mieV: 4.0
    property real mieZenithLength: 1.25e3
    property real numMolecules: 2.542e25
    property real rayleigh: 1
    property real rayleighZenithLength: 8.4e3
    property real refractiveIndex: 1
    property real sunAngularDiameterDegrees: 0.01
    property real sunIntensityFactor: 1000.0
    property real sunIntensityFalloffSteepness: 1.5
    property real tonemapWeighting: 9.5
    property real turbidity: 1.25


    MetalRoughMaterial {
        id: matBase
    }
    property Camera camera



    Material {
        id: materialSky

        parameters: [
            Parameter {
                name: "projectionMatrix"
                value: root.camera.projectionMatrix
            },
            Parameter {
                name: "depolarizationFactor"
                value: root.depolarizationFactor
            },
            Parameter {
                name: "luminance"
                value: root.luminance
            },
            Parameter {
                name: "mieCoefficient"
                value: root.mieCoefficient
            },
            Parameter {
                name: "mieDirectionalG"
                value: root.mieDirectionalG
            },
            Parameter {
                name: "mieV"
                value: root.mieV
            },
            Parameter {
                name: "mieZenithLength"
                value: root.mieZenithLength
            },
            Parameter {
                name: "numMolecules"
                value: root.numMolecules
            },
            Parameter {
                name: "rayleigh"
                value: root.rayleigh
            },
            Parameter {
                name: "rayleighZenithLength"
                value: root.rayleighZenithLength
            },
            Parameter {
                name: "refractiveIndex"
                value: root.refractiveIndex
            },
            Parameter {
                name: "sunAngularDiameterDegrees"
                value: root.sunAngularDiameterDegrees
            },
            Parameter {
                name: "sunIntensityFactor"
                value: root.sunIntensityFactor
            },
            Parameter {
                name: "sunIntensityFalloffSteepness"
                value: root.sunIntensityFalloffSteepness
            },
            Parameter {
                name: "tonemapWeighting"
                value: root.tonemapWeighting
            },
            Parameter {
                name: "turbidity"
                value: root.turbidity
            },
            Parameter {
                name: "sunPosition"
                value: root.sunPosition
            },
            Parameter {
                name: "primaries"
                value: root.primaries
            },
            Parameter {
                name: "mieKCoefficient"
                value: root.mieKCoefficient
            },
            Parameter {
                name: "cameraPos"
                value: camera.position
            }
        ]

        AlphaCoverage {
            id: alphaCoverage
        }
        FilterKey {
            id: forward
            name: "renderingStyle"
            value: "forward"
        }
        effect: Effect {
            property string vertex: "qrc:/assets/shader/Sky.vert"
            property string fragment: "qrc:/assets/shader/Sky.frag"

            ShaderProgram {
                id: gl3Shader
                vertexShaderCode: loadSource(parent.vertex)
                fragmentShaderCode: loadSource(parent.fragment)
                onStatusChanged: print("status ", status)
                onLogChanged: print("log", log)
            }
            techniques: [
                // OpenGL 4.5
                Technique {
                    filterKeys: [forward]
                    graphicsApiFilter {
                        api: GraphicsApiFilter.OpenGL
                        profile: GraphicsApiFilter.CoreProfile
                        majorVersion: 4
                        minorVersion: 5
                    }
                    renderPasses: RenderPass {
                        shaderProgram: gl3Shader
                        //   renderStates: [alphaCoverage]
                    }
                }
            ]
        }
    }

    Transform {
        id: transform
        scale: 1
    }

    Mesh {
        id: mesh
        source: "qrc:/assets/obj/Skybox.obj"
    }
    //    Entity {
    //        components: [
    //            Transform {},
    //            ConeMesh {},
    //            MetalRoughMaterial {}
    //        ]
    //    }
}
