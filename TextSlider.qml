import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

ColumnLayout {
    id: root
    property alias slider: slider
    property alias text: headline.text
    property var value
    onValueChanged: {
        print(text, root.value)
        slider.value = value
    }

    spacing:10
    Text {
        id: headline
        text: qsTr("Headline")
        color: "gray"
        width: parent.width
    }
    Slider {
        id: slider
        width: parent.width
        onMoved:{
            root.value = slider.value
        }
    }
}
