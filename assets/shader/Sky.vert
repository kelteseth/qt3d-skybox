#version 450
#define FP highp

out vec3 vWorldPosition;

//in mat4  projectionMatrix;
attribute  vec3 vertexPosition;
varying vec3 worldPosition;
uniform mat4 modelMatrix;
//uniform mat4 modelView;
uniform mat4 mvp;

void main()
{
    // Transform position, normal, and tangent to world coords
    worldPosition = vec3(modelMatrix * vec4(vertexPosition, 1.0));
    vWorldPosition = worldPosition.xyz;
    // Calculate vertex position in clip coordinates
    gl_Position = mvp * vec4(worldPosition, 1.0);
}
