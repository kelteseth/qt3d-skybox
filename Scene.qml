import QtQuick 2.15
import Qt3D.Core 2.15
import Qt3D.Render 2.15
import Qt3D.Input 2.15
import Qt3D.Extras 2.15

import Util 1.0

Entity {
    id: root

    property var mainCamera: mainCamera
    property var sky: skybox

    // Use the renderer configuration specified in ForwardRenderer.qml
    // and render from the mainCamera
    components: [
        RenderSettings {
            activeFrameGraph: ForwardRenderer {
                camera: mainCamera
                clearColor: Qt.rgba(0, 0, 0, 1)
              //  showDebugOverlay: true
            }
        },
        // Event Source will be set by the Qt3DQuickWindow
        InputSettings {}
    ]

    BasicCamera {
        id: mainCamera
        position: Qt.vector3d(-5.17253, 2.95727, 6.65948)
        viewCenter: Qt.vector3d(6.73978, -2.50545, -10.6525)
    }

    FirstPersonCameraController {
        camera: mainCamera
    }

    Lights {}

    Sky {
        id:skybox
        camera: mainCamera
    }

    Entity {
        id: floor

        components: [
            Mesh {
                source: "qrc:/assets/obj/plane-10x10.obj"
            },

            TexturedMetalRoughMaterial {
                baseColor: TextureLoader {
                    source: Util.appBasePath + "/assets/textures/ceramic_small_diamond/ceramic_small_diamond_basecolor.png"
                    format: Texture.SRGB8_Alpha8
                    generateMipMaps: true
                }
                metalness: TextureLoader {
                    source: Util.appBasePath + "/assets/textures/ceramic_small_diamond/ceramic_small_diamond_metallic.png"
                    generateMipMaps: true
                }
                roughness: TextureLoader {
                    source: Util.appBasePath + "/assets/textures/ceramic_small_diamond/ceramic_small_diamond_roughness.png"
                    generateMipMaps: true
                }
                normal: TextureLoader {
                    source: Util.appBasePath + "/assets/textures/ceramic_small_diamond/ceramic_small_diamond_normal.png"
                    generateMipMaps: true
                }
                ambientOcclusion: TextureLoader {
                    source: Util.appBasePath + "/assets/textures/ceramic_small_diamond/ceramic_small_diamond_ambient_occlusion.png"
                }
            }
        ]
    }

    Mesh {
        id: matSphere
        source: Util.appBasePath + "/assets/obj/material-sphere.obj"
    }

    Entity {
        id: sphere1

        components: [matSphere1Transform, matSphere, matSphere1Material]

        Transform {
            id: matSphere1Transform
            translation: Qt.vector3d(-3, 0, 0)
            rotationY: -90
        }

        MetalRoughMaterial {
            id: matSphere1Material
            baseColor: "grey"
        }
    }
}
