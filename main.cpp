#include "util.h"
#include <QGuiApplication>
#include <QOpenGLContext>
#include <QQmlApplicationEngine>
#include <QSurfaceFormat>
#include <chrono>
#include <thread>

int main(int argc, char *argv[]) {
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QCoreApplication::setAttribute(Qt::AA_UseDesktopOpenGL);
  QGuiApplication app(argc, argv);
//  QSurfaceFormat fmt;
//  fmt.setDepthBufferSize(24);
//  if (QOpenGLContext::openGLModuleType() == QOpenGLContext::LibGL) {
//    fmt.setVersion(3, 3);
//    fmt.setProfile(QSurfaceFormat::CompatibilityProfile);
//  } else {
//    fmt.setVersion(3, 0);
//  }
//  qDebug() << fmt.version();
//  QSurfaceFormat::setDefaultFormat(fmt);

  // This gives us nice clickable output in QtCreator
  qSetMessagePattern("%{if-category}%{category}: %{endif}%{message}\n   Loc: [%{file}:%{line}]");


  QQmlApplicationEngine engine;

  Util util;
  qmlRegisterType<Util>("Util", 1, 0, "Util");
  qmlRegisterAnonymousType<Util>("Util", 1);
  qmlRegisterSingletonInstance<Util>("Util", 1, 0, "Util", &util);
  engine.load("qrc:/main.qml");

  return app.exec();
}
