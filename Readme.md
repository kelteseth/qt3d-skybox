### Qt3d Sky shader port of threejs examples

Based on https://threejs.org/examples/webgl_shaders_sky.html

Tested with the latest [Qt 5.15](https://www.qt.io/download-qt-installer) version!

### Important!
Copy the assets folder to you build dir!